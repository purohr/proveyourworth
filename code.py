
"""
Developed on xed and console
Linux and python
telegram @purohr
gitlab: https://gitlab.com/purohr

"""
import requests 
from bs4 import BeautifulSoup
from PIL import Image,ImageDraw
from pathlib import Path
from typing import Tuple
import json
import info

class Connetion():
    def __init__(self, start_page: str, payload_page: str, var_activate_page: str) -> None:
        '''Contructor de la clase, recibe las constantes de la pagina, link de las paginas con las que se interatuara '''
        self.session=requests.Session()
        self.start_page=start_page
        self.payload_page=payload_page
        self.var_activate_page=var_activate_page
    
    def create_session(self) -> str:
        '''Metodo que crea la sección con la pagina y obtiene el identificador de la cookie generada, luego se muestra con el print y se retorna al main'''
        session=self.session
        session.get(self.start_page)
        print(f'Code Hash: {session.cookies.get("PHPSESSID")}')
        return session.cookies.get('PHPSESSID')
        
    def read_full_hash(self) -> str:
        '''Método que obtiene el codigo hash para activar el payload de la pagina, hacemos scraping, buscamos las labels y obtenemos un str del codigo de activación'''
        request=self.session.get(self.start_page)
        soup=BeautifulSoup(request.text, 'html.parser')
        return soup.find('input',{'name':'statefulhash'})['value']
        
    def activate_page(self) -> None:
        '''método usado para activar la pagina, uniendo el link de var_activate_page con el hash obtenido mediante scraping, una vez realizado ya da opcion para descargar la imagen para continuar con el reto'''        
        full_hash=self.read_full_hash()
        print(full_hash)
        self.session.get(self.var_activate_page+f'={full_hash}')
        print(f'Link with activate Hash: \n{self.var_activate_page}"="{full_hash}')
        #return None
              
    def load_image(self) -> bytes:
        '''Método para descargar la imagen en un stream  de bytes, retorna el stream para ser tratado
        '''
        request=self.session.get(self.payload_page, stream=True)
        #print(request.content)
        return request.raw
        
    def post_to_back(self, data, files) -> None:
        '''Con este método enviamos los documentos necesarios para completar la prueba,  '''
        payload_headers=self.session.get(self.payload_page).headers["X-Post-Back-To"]# hacemos un get a la pagina del payload, y obtenemos solo el link, para enviar la doumentación
        post_payload=f'{payload_headers}'
        print(f'\nPayload: {self.session.get(self.payload_page).headers}\n')# mostramos en pantalla el payload completo, y miramos cuales son los documentos necesarios y cuales son los requerimientos que se deben enviar.
        
        print(f'\nReaver: {self.session.get(post_payload).headers}\n')#paso clave, tenemos que hacer primero un get al link del Reaver, para activar la seccion, para luego se active el envio de la información
        #print(f'\nData: {data}')
        #print(f'\nfiles:\n{files}\n')
        request = self.session.post(post_payload, json=data, files=files, data=data)# Se envian los datos, revisar que contiene cada variable. Para enviar los archivos lo mejor es mandar la trama de bytes para evitar errores.
        
        print(f'\nCookies: \n{request.cookies}\n')
        print(request.text)#link del resultado del post
        print(request.status_code)# codigo de estado del envio de los datos
        
        
        
class ImageProcess():
    def __init__(self, image_raw: bytes)-> None:
    '''
    Constructor de la clase, recibe un stream de bytes con la imagen y la abre para procesarla
    '''
        self.image_opened=Image.open(image_raw)
        #draw=ImageDraw.Draw(image)
        
        
    def text_position(self, image_raw: bytes) -> Tuple[int,int]:
    '''
    Posición del texto en la imagen, recibe la imagen abierta 
    '''
        #image=Image.open(image_raw)        
        image=self.image_opened
        print(f'Size image: {image.size}')#muestra el tamaño de la imagen
        position_column=int(input('Enter text position in column:\n'))
        position_row=int(input('Enter text position in row:\n'))
        #if (0<position_column >= image.size[0] or 0<position_row >= image.size[1]) and position_column.isdigit() and position_row.isdigit()
        image.close()
        return (position_column, position_row)
        
    def draw_image_text(self, text: str)-> None:
        '''
        Pone en la imagen el texto del  desarrollador el hash y el correo
        '''
        image=self.image_opened
        draw= ImageDraw.Draw(image)
        draw.text((40, image.size[1]-200), text, fill=(1024,1024,0))
        image.save('image.jpg','JPEG')
        print(f'image saved')
        image.close()
        
        
if __name__ == '__main__':
    # define variables      
    start_page= 'http://www.proveyourworth.net/level3/start'
    payload_page= 'http://www.proveyourworth.net/level3/payload'
    var_activate_page= 'http://www.proveyourworth.net/level3/activate?statefulhash'
    # set path to files
    file_path=Path('./')
    #files to send
    files_to_send = {
        'code':open(file_path / 'code.py','rb'),#r=read b=binary
        'resume':open(file_path / 'resume.pdf','rb'),
        'image':open(file_path / 'image.jpg','rb')
    }

    #begin hack the page
    print('*'*10 + 'Start PHPSESSID ' + '*'*10) 
    create_conection= Connetion(start_page, payload_page, var_activate_page)    
    hash_php=create_conection.create_session()
    print('*'*10 + 'Create Session: ' + '*'*10)
    print(hash_php) 
    full_hash=create_conection.read_full_hash()
    print('\n'+'*'*10 + 'Fullhash captured ' + '*'*10)
    print(f'\nFullhash: {full_hash}') 
    image_text=f'{info.persona['name']}, \nHash:{full_hash} \n info.persona['email'] \ninfo.persona['title']'
    print('\n'+'*'*10 + 'Creating connection... ' + '*'*10) 
    create_conection.activate_page()
    print('\n'+'*'*10 + 'Connection Done! ' + '*'*10) 
    image_raw=create_conection.load_image()    
    image_object=ImageProcess(image_raw)
    print('\n'+'*'*10 + 'Getting image in bytes...' + '*'*10) 
    image_object.draw_image_text(image_text)
    print('\n'+'*'*10 + 'All images saved' + '*'*10)     
    #create_conection.post_to_back(info.persona , files_to_send)
    print('*'*10 + 'All files send' + '*'*10)
    print('*'*10 + 'The program finished' + '*'*10)            
                                 
  
    
    
    
    
    
                    
        
        
